let player = {
    x: 750 - 20,
    y: 450 - 20,
    degree: 0,
    collide: () => {
        if (player.x - 50 < -10) {
            player.x = 40;
        }
        if (player.y - 50 < -10) {
            player.y = 40;
        }
        if (player.x + 50 > 1510) {
            player.x = 1460;
        }
        if (player.y + 50 > 910) {
            player.y = 860;
        }
    },
    move: () => {
        if ((movement_buffer[0] && movement_buffer[2]) || (movement_buffer[0] && movement_buffer[3]) || (movement_buffer[1] && movement_buffer[2]) || (movement_buffer[1] && movement_buffer[3])) {
            if (movement_buffer[0] && movement_buffer[2]) {
                player.y -= 0.707;
                player.x -= 0.707;
            } else if (movement_buffer[0] && movement_buffer[3]) {
                player.y -= 0.707;
                player.x += 0.707;
            } else if (movement_buffer[1] && movement_buffer[2]) {
                player.y += 0.707;
                player.x -= 0.707;
            } else if (movement_buffer[1] && movement_buffer[3]) {
                player.y += 0.707;
                player.x += 0.707;
            }
        } else {
            if (movement_buffer[0]) {
                player.y -= 1;
            }
            if (movement_buffer[1]) {
                player.y += 1;
            }
            if (movement_buffer[2]) {
                player.x -= 1;
            }
            if (movement_buffer[3]) {
                player.x += 1;
            }
        }

        player.collide();
    },
    rotate: () => {
        player.degree = Math.atan2(-((mx - gameBoard.x) - player.x), ((my - gameBoard.y) - player.y));
        ctx.save();
        ctx.translate(player.x, player.y);
        ctx.rotate(player.degree);
        player.draw();
        ctx.restore();
    },
    draw: () => {
        ctx.fillStyle = 'black';
        ctx.strokeStyle = '#35354d';
        ctx.lineWidth = 5;
        ctx.fillStyle = '#7d5d4f';
        ctx.fillRect(-30, 23, 20, 40);
        ctx.strokeRect(-28, 23, 20, 40);
        ctx.fillStyle = '#7d5d4f';
        ctx.beginPath();
        ctx.arc(20, 40, 15, 0, 2 * Math.PI);
        ctx.fill(); 
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(-17, 60, 15, 0, 2 * Math.PI);
        ctx.fill(); 
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(0, 0, 40, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.save();
        ctx.scale(1, -1);
        ctx.drawImage(gun, -12.5, -118, 25, 80);
        ctx.restore();
        ctx.strokeRect(-12.5, 38, 25, 80);
    }
};