//setup variables
let ctx = document.getElementById('gc').getContext('2d');
let mx, my;
mx = my = 0;
let gameBoard;
let movement_buffer = [0,0,0,0];
let gun = document.getElementById('gun');

//event listeners
document.addEventListener('keydown', (e) => {
    switch(e.which) {
        case 87:
            movement_buffer[0] = 1;
            break;
        case 83:
            movement_buffer[1] = 1;
            break;
        case 65:
            movement_buffer[2] = 1;
            break;
        case 68:
            movement_buffer[3] = 1;
    }
});
document.addEventListener('keyup', (e) => {
    switch(e.which) {
        case 87:
            movement_buffer[0] = 0;
            break;
        case 83:
            movement_buffer[1] = 0;
            break;
        case 65:
            movement_buffer[2] = 0;
            break;
        case 68:
            movement_buffer[3] = 0;
    }
});

document.addEventListener('mousemove', (e) => {
    mx = e.clientX;
    my = e.clientY;
});

//functions
function game() {
    gameBoard = document.getElementById('gc').getBoundingClientRect();
    ctx.clearRect(0, 0, 1500, 900);
    drawBackground();
    player.move();
    player.rotate();
    hudWidth();
}

function drawBackground() {
    ctx.lineWidth = 3;
    ctx.strokeStyle = '#708657';
    ctx.fillStyle='#778F5B';
    ctx.fillRect(0, 0, 1500, 900);
    for (let i = 900 / 17; i > 0; i--) {
        ctx.beginPath();
        ctx.moveTo(0, i * (900 / 17));
        ctx.lineTo(1500, i * (900 / 17));
        ctx.stroke();
    }
    for (let i = 1500 / 27; i > 0; i--) {
        ctx.beginPath();
        ctx.moveTo(i * (1500 / 27), 0);
        ctx.lineTo(i * (1500 / 27), 900);
        ctx.stroke();             
    }
}

function hudWidth() {
    document.getElementById('HUD').style.width = ((screen.width - 1500) / 2) + 'px';
}

//game
setInterval(game, 1000/210);